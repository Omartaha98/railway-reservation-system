﻿namespace LoginUser
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form3));
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button16 = new System.Windows.Forms.Button();
            this.panelDropDown = new System.Windows.Forms.Panel();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button14 = new System.Windows.Forms.Button();
            this.SidePanel1 = new System.Windows.Forms.Panel();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.button8 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.driverToTrainUserControl11 = new LoginUser.DriverToTrainUserControl1();
            this.homeInterfaceUserControl11 = new LoginUser.HomeInterfaceUserControl1();
            this.deleteTripUserControl11 = new LoginUser.DeleteTripUserControl1();
            this.driversShiftsUserControl11 = new LoginUser.DriversShiftsUserControl1();
            this.tripAddDataUserControl1 = new LoginUser.TripAddDataUserControl();
            this.updateTripUserControl11 = new LoginUser.UpdateTripUserControl1();
            this.viewLoginUserControl11 = new LoginUser.ViewLoginUserControl1();
            this.viewUserControl11 = new LoginUser.ViewUserControl1();
            this.addTrainUserControl11 = new LoginUser.AddTrainUserControl1();
            this.panel1.SuspendLayout();
            this.panelDropDown.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(318, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 17);
            this.label1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.panel1.Controls.Add(this.button16);
            this.panel1.Controls.Add(this.panelDropDown);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.button14);
            this.panel1.Controls.Add(this.SidePanel1);
            this.panel1.Controls.Add(this.button7);
            this.panel1.Controls.Add(this.button6);
            this.panel1.Controls.Add(this.button5);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Location = new System.Drawing.Point(-1, -2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(275, 603);
            this.panel1.TabIndex = 1;
            // 
            // button16
            // 
            this.button16.FlatAppearance.BorderSize = 0;
            this.button16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button16.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button16.ForeColor = System.Drawing.Color.White;
            this.button16.Image = ((System.Drawing.Image)(resources.GetObject("button16.Image")));
            this.button16.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button16.Location = new System.Drawing.Point(20, 508);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(243, 54);
            this.button16.TabIndex = 13;
            this.button16.Text = "     Driver to Train";
            this.button16.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // panelDropDown
            // 
            this.panelDropDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.panelDropDown.Controls.Add(this.button10);
            this.panelDropDown.Controls.Add(this.button9);
            this.panelDropDown.Controls.Add(this.button1);
            this.panelDropDown.Location = new System.Drawing.Point(23, 124);
            this.panelDropDown.MaximumSize = new System.Drawing.Size(234, 135);
            this.panelDropDown.MinimumSize = new System.Drawing.Size(234, 54);
            this.panelDropDown.Name = "panelDropDown";
            this.panelDropDown.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.panelDropDown.Size = new System.Drawing.Size(234, 54);
            this.panelDropDown.TabIndex = 7;
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button10.Dock = System.Windows.Forms.DockStyle.Top;
            this.button10.FlatAppearance.BorderSize = 0;
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.ForeColor = System.Drawing.Color.White;
            this.button10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button10.Location = new System.Drawing.Point(0, 96);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(234, 36);
            this.button10.TabIndex = 8;
            this.button10.Text = "     Log Out\r\n";
            this.button10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button9.Dock = System.Windows.Forms.DockStyle.Top;
            this.button9.FlatAppearance.BorderSize = 0;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.ForeColor = System.Drawing.Color.White;
            this.button9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button9.Location = new System.Drawing.Point(0, 54);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(234, 42);
            this.button9.TabIndex = 7;
            this.button9.Text = "     Trip Data";
            this.button9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click_1);
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(234, 54);
            this.button1.TabIndex = 6;
            this.button1.Text = "     Home";
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(13, 57);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(231, 61);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 14;
            this.pictureBox1.TabStop = false;
            // 
            // button14
            // 
            this.button14.FlatAppearance.BorderSize = 0;
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button14.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button14.ForeColor = System.Drawing.Color.White;
            this.button14.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button14.Location = new System.Drawing.Point(13, 568);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(36, 34);
            this.button14.TabIndex = 13;
            this.button14.Text = "?";
            this.button14.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button14.UseVisualStyleBackColor = true;
            // 
            // SidePanel1
            // 
            this.SidePanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(8)))), ((int)(((byte)(55)))));
            this.SidePanel1.Location = new System.Drawing.Point(3, 124);
            this.SidePanel1.Name = "SidePanel1";
            this.SidePanel1.Size = new System.Drawing.Size(10, 54);
            this.SidePanel1.TabIndex = 13;
            // 
            // button7
            // 
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.Color.White;
            this.button7.Image = ((System.Drawing.Image)(resources.GetObject("button7.Image")));
            this.button7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button7.Location = new System.Drawing.Point(23, 448);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(243, 54);
            this.button7.TabIndex = 12;
            this.button7.Text = "     New Train";
            this.button7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.Image = ((System.Drawing.Image)(resources.GetObject("button6.Image")));
            this.button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.Location = new System.Drawing.Point(20, 394);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(243, 54);
            this.button6.TabIndex = 11;
            this.button6.Text = "     DriverShifts";
            this.button6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Image = ((System.Drawing.Image)(resources.GetObject("button5.Image")));
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.Location = new System.Drawing.Point(20, 340);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(243, 54);
            this.button5.TabIndex = 10;
            this.button5.Text = "     Driver Info";
            this.button5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(23, 286);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(243, 54);
            this.button4.TabIndex = 9;
            this.button4.Text = "     Delete Trip";
            this.button4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(23, 232);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(243, 54);
            this.button3.TabIndex = 8;
            this.button3.Text = "     Update Trip";
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(23, 178);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(243, 54);
            this.button2.TabIndex = 7;
            this.button2.Text = "     New Trip";
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.button8);
            this.panel2.Controls.Add(this.button13);
            this.panel2.Controls.Add(this.button12);
            this.panel2.Controls.Add(this.button11);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(922, 41);
            this.panel2.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(79, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(195, 17);
            this.label2.TabIndex = 17;
            this.label2.Text = "Train Reservation System";
            // 
            // button8
            // 
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.ForeColor = System.Drawing.Color.White;
            this.button8.Image = ((System.Drawing.Image)(resources.GetObject("button8.Image")));
            this.button8.Location = new System.Drawing.Point(22, 3);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(28, 34);
            this.button8.TabIndex = 15;
            this.button8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button13
            // 
            this.button13.FlatAppearance.BorderSize = 0;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.ForeColor = System.Drawing.Color.White;
            this.button13.Image = ((System.Drawing.Image)(resources.GetObject("button13.Image")));
            this.button13.Location = new System.Drawing.Point(869, 3);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(41, 34);
            this.button13.TabIndex = 16;
            this.button13.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button12
            // 
            this.button12.FlatAppearance.BorderSize = 0;
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.ForeColor = System.Drawing.Color.White;
            this.button12.Image = ((System.Drawing.Image)(resources.GetObject("button12.Image")));
            this.button12.Location = new System.Drawing.Point(825, 3);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(28, 34);
            this.button12.TabIndex = 15;
            this.button12.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button12.UseVisualStyleBackColor = true;
            // 
            // button11
            // 
            this.button11.FlatAppearance.BorderSize = 0;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.ForeColor = System.Drawing.Color.White;
            this.button11.Image = ((System.Drawing.Image)(resources.GetObject("button11.Image")));
            this.button11.Location = new System.Drawing.Point(781, 3);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(28, 34);
            this.button11.TabIndex = 14;
            this.button11.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button11.UseVisualStyleBackColor = true;
            // 
            // timer1
            // 
            this.timer1.Interval = 15;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // driverToTrainUserControl11
            // 
            this.driverToTrainUserControl11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("driverToTrainUserControl11.BackgroundImage")));
            this.driverToTrainUserControl11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.driverToTrainUserControl11.Location = new System.Drawing.Point(271, 43);
            this.driverToTrainUserControl11.Name = "driverToTrainUserControl11";
            this.driverToTrainUserControl11.Size = new System.Drawing.Size(651, 558);
            this.driverToTrainUserControl11.TabIndex = 11;
            this.driverToTrainUserControl11.Load += new System.EventHandler(this.driverToTrainUserControl11_Load);
            // 
            // homeInterfaceUserControl11
            // 
            this.homeInterfaceUserControl11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("homeInterfaceUserControl11.BackgroundImage")));
            this.homeInterfaceUserControl11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.homeInterfaceUserControl11.Location = new System.Drawing.Point(271, 42);
            this.homeInterfaceUserControl11.Name = "homeInterfaceUserControl11";
            this.homeInterfaceUserControl11.Size = new System.Drawing.Size(651, 558);
            this.homeInterfaceUserControl11.TabIndex = 10;
            // 
            // deleteTripUserControl11
            // 
            this.deleteTripUserControl11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("deleteTripUserControl11.BackgroundImage")));
            this.deleteTripUserControl11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.deleteTripUserControl11.Location = new System.Drawing.Point(271, 43);
            this.deleteTripUserControl11.Name = "deleteTripUserControl11";
            this.deleteTripUserControl11.Size = new System.Drawing.Size(651, 558);
            this.deleteTripUserControl11.TabIndex = 9;
            // 
            // driversShiftsUserControl11
            // 
            this.driversShiftsUserControl11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("driversShiftsUserControl11.BackgroundImage")));
            this.driversShiftsUserControl11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.driversShiftsUserControl11.Location = new System.Drawing.Point(271, 43);
            this.driversShiftsUserControl11.Name = "driversShiftsUserControl11";
            this.driversShiftsUserControl11.Size = new System.Drawing.Size(651, 558);
            this.driversShiftsUserControl11.TabIndex = 8;
            // 
            // tripAddDataUserControl1
            // 
            this.tripAddDataUserControl1.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.tripAddDataUserControl1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tripAddDataUserControl1.BackgroundImage")));
            this.tripAddDataUserControl1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tripAddDataUserControl1.Location = new System.Drawing.Point(271, 42);
            this.tripAddDataUserControl1.Name = "tripAddDataUserControl1";
            this.tripAddDataUserControl1.Size = new System.Drawing.Size(651, 558);
            this.tripAddDataUserControl1.TabIndex = 7;
            // 
            // updateTripUserControl11
            // 
            this.updateTripUserControl11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("updateTripUserControl11.BackgroundImage")));
            this.updateTripUserControl11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.updateTripUserControl11.Location = new System.Drawing.Point(271, 42);
            this.updateTripUserControl11.Name = "updateTripUserControl11";
            this.updateTripUserControl11.Size = new System.Drawing.Size(651, 558);
            this.updateTripUserControl11.TabIndex = 6;
            // 
            // viewLoginUserControl11
            // 
            this.viewLoginUserControl11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("viewLoginUserControl11.BackgroundImage")));
            this.viewLoginUserControl11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.viewLoginUserControl11.Location = new System.Drawing.Point(271, 42);
            this.viewLoginUserControl11.Name = "viewLoginUserControl11";
            this.viewLoginUserControl11.Size = new System.Drawing.Size(651, 558);
            this.viewLoginUserControl11.TabIndex = 5;
            // 
            // viewUserControl11
            // 
            this.viewUserControl11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("viewUserControl11.BackgroundImage")));
            this.viewUserControl11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.viewUserControl11.Location = new System.Drawing.Point(271, 42);
            this.viewUserControl11.Name = "viewUserControl11";
            this.viewUserControl11.Size = new System.Drawing.Size(651, 558);
            this.viewUserControl11.TabIndex = 4;
            // 
            // addTrainUserControl11
            // 
            this.addTrainUserControl11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("addTrainUserControl11.BackgroundImage")));
            this.addTrainUserControl11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.addTrainUserControl11.Location = new System.Drawing.Point(271, 42);
            this.addTrainUserControl11.Name = "addTrainUserControl11";
            this.addTrainUserControl11.Size = new System.Drawing.Size(651, 558);
            this.addTrainUserControl11.TabIndex = 3;
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(922, 601);
            this.Controls.Add(this.driverToTrainUserControl11);
            this.Controls.Add(this.homeInterfaceUserControl11);
            this.Controls.Add(this.deleteTripUserControl11);
            this.Controls.Add(this.driversShiftsUserControl11);
            this.Controls.Add(this.tripAddDataUserControl1);
            this.Controls.Add(this.updateTripUserControl11);
            this.Controls.Add(this.viewLoginUserControl11);
            this.Controls.Add(this.viewUserControl11);
            this.Controls.Add(this.addTrainUserControl11);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form3";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TrainReservationSystem";
            this.panel1.ResumeLayout(false);
            this.panelDropDown.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel SidePanel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel panelDropDown;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private AddTrainUserControl1 addTrainUserControl11;
        private ViewUserControl1 viewUserControl11;
        private ViewLoginUserControl1 viewLoginUserControl11;
        private UpdateTripUserControl1 updateTripUserControl11;
        private TripAddDataUserControl tripAddDataUserControl1;
        private DriversShiftsUserControl1 driversShiftsUserControl11;
        private DeleteTripUserControl1 deleteTripUserControl11;
        private HomeInterfaceUserControl1 homeInterfaceUserControl11;
        private System.Windows.Forms.Button button16;
        private DriverToTrainUserControl1 driverToTrainUserControl11;
    }
}