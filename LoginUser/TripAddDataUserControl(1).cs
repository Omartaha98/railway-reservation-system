﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace LoginUser
{
    public partial class TripAddDataUserControl : UserControl
    {
        public TripAddDataUserControl()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if ( textBox1.Text == "" || textBox4.Text == "" || textBox5.Text =="" || textBox2.Text =="" || textBox3.Text =="")
                {
                    MessageBox.Show("No entered elements!", ("Missing Inputs"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    SqlConnection con = new SqlConnection("Data Source=DESKTOP-EIQMB92;Initial Catalog=NewUserLogin;Integrated Security=True");
                    con.Open();
                    SqlCommand cmd = new SqlCommand("insert into Trip (Trip_Number,DepartialCity,ArrivalCity,DepartTime,ArrivalTime,DepartDate,Ticket_Price,Train_ID) values (@varTrip_ID,@varDepartialCity,@varArrivalCity,@varDepartTime,@varArrivalTime,@varDepartDate,@varTicket_Price,@varTrain_ID)", con);
                    SqlParameter parTrip_ID = new SqlParameter("@varTrip_ID", textBox1.Text);
                    cmd.Parameters.Add(parTrip_ID);
                    SqlParameter parDepartialCity = new SqlParameter("@varDepartialCity", textBox2.Text);
                    cmd.Parameters.Add(parDepartialCity);
                    SqlParameter parArrivalCity = new SqlParameter("@varArrivalCity", textBox3.Text);
                    cmd.Parameters.Add(parArrivalCity);
                    SqlParameter parDepartTime = new SqlParameter("@varDepartTime", TimePicker1.Text);
                    cmd.Parameters.Add(parDepartTime);
                    SqlParameter parArrivalTime = new SqlParameter("@varArrivalTime", TimePicker2.Text);
                    cmd.Parameters.Add(parArrivalTime);
                    SqlParameter parDepartDate = new SqlParameter("@varDepartDate", datePicker3.Text);
                    cmd.Parameters.Add(parDepartDate);
                    SqlParameter parTicket_Price = new SqlParameter("@varTicket_Price", textBox4.Text);
                    cmd.Parameters.Add(parTicket_Price);
                    SqlParameter parTrain_ID = new SqlParameter("@varTrain_ID", textBox5.Text);
                    cmd.Parameters.Add(parTrain_ID);
                    cmd.ExecuteNonQuery();
                    con.Close();
                    MessageBox.Show("Data saved correctly!", ("Sucessfull entry"), MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Same Data already exists!", ("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        private void TripAddDataUserControl_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=DESKTOP-EIQMB92;Initial Catalog=NewUserLogin;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Trip", con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable T = new DataTable();
            T.Columns.Add("TripNumber");
            T.Columns.Add("DepartCity");
            T.Columns.Add("ArrivalCity");
            T.Columns.Add("DepartTime");
            T.Columns.Add("ArrivalTime");
            T.Columns.Add("DepartDate");
            T.Columns.Add("TicketPrice");
            T.Columns.Add("TrainNumber");
            DataRow row;
            while (reader.Read())
            {
                row = T.NewRow();
                row["TripNumber"] = reader["Trip_Number"];
                row["DepartCity"] = reader["DepartialCity"];
                row["ArrivalCity"] = reader["ArrivalCity"];
                row["DepartTime"] = reader["DepartTime"];
                row["ArrivalTime"] = reader["ArrivalTime"];
                row["DepartDate"] = reader["DepartDate"];
                row["TicketPrice"] = reader["Ticket_Price"];
                row["TrainNumber"] = reader["Train_ID"];
                T.Rows.Add(row);
            }

            reader.Close();
            con.Close();
            dataGridView1.DataSource = T;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=DESKTOP-EIQMB92;Initial Catalog=NewUserLogin;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Trip", con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable T = new DataTable();
            T.Columns.Add("TripNumber");
            T.Columns.Add("DepartCity");
            T.Columns.Add("ArrivalCity");
            T.Columns.Add("DepartTime");
            T.Columns.Add("ArrivalTime");
            T.Columns.Add("DepartDate");
            T.Columns.Add("TicketPrice");
            T.Columns.Add("TrainNumber");
            DataRow row;
            while (reader.Read())
            {
                row = T.NewRow();
                row["TripNumber"] = reader["Trip_Number"];
                row["DepartCity"] = reader["DepartialCity"];
                row["ArrivalCity"] = reader["ArrivalCity"];
                row["DepartTime"] = reader["DepartTime"];
                row["ArrivalTime"] = reader["ArrivalTime"];
                row["DepartDate"] = reader["DepartDate"];
                row["TicketPrice"] = reader["Ticket_Price"];
                row["TrainNumber"] = reader["Train_ID"];
                T.Rows.Add(row);
            }

            reader.Close();
            con.Close();
            dataGridView1.DataSource = T;
        }
        

        

        
    }
}
