﻿using LoginUser.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace LoginUser
{
    public partial class Form3 : Form
    {
        private bool isCollapsed;
        public Form3()
        {

            InitializeComponent();
         


        }

        private void button2_Click(object sender, EventArgs e)
        {

            SidePanel1.Height = button2.Height;
            SidePanel1.Top = button2.Top;
            tripAddDataUserControl1.BringToFront();

        }

        private void button13_Click(object sender, EventArgs e)
        {
            if (System.Windows.Forms.Application.MessageLoop)
            {
                System.Windows.Forms.Application.Exit();
            }
            else
            {
                System.Environment.Exit(1);
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            welcom1.BringToFront();
            SidePanel1.Top = panelDropDown.Top;
            timer1.Start();





        }

        private void button3_Click(object sender, EventArgs e)
        {
            SidePanel1.Height = button3.Height;
            SidePanel1.Top = button3.Top;
            updateTripUserControl11.BringToFront();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            SidePanel1.Height = button4.Height;
            SidePanel1.Top = button4.Top;
            deleteTripUserControl11.BringToFront();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            SidePanel1.Height = button5.Height;
            SidePanel1.Top = button5.Top;
            driverInfoUserControl11.BringToFront();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            SidePanel1.Height = button6.Height;
            SidePanel1.Top = button6.Top;
            driversShiftsUserControl11.BringToFront();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            SidePanel1.Height = button7.Height;
            SidePanel1.Top = button7.Top;
            addTrainUserControl11.BringToFront();
        }




        private void timer1_Tick(object sender, EventArgs e)
        {
            if (isCollapsed)
            {
                button1.Image = Resources.Collapse_Arrow_20px;
                panelDropDown.Height += 10;
                if (panelDropDown.Size == panelDropDown.MaximumSize)
                {
                    timer1.Stop();
                    isCollapsed = false;
                }
            }
            else
            {
                button1.Image = Resources.Expand_Arrow_20px;
                panelDropDown.Height -= 10;
                if (panelDropDown.Size == panelDropDown.MinimumSize)
                {
                    timer1.Stop();
                    isCollapsed = true;
                }
            }
        }

        private void button9_Click_1(object sender, EventArgs e)
        {
            viewLoginUserControl11.BringToFront();
        }

        private void button10_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("Are You sure To Log Out?", ("Log Out"), MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
            {
                this.Hide();
                Form1 f = new Form1();

                f.ShowDialog();
            }

        }

        private void button16_Click(object sender, EventArgs e)
        {
            SidePanel1.Height = button16.Height;
            SidePanel1.Top = button16.Top;
            driverToTrainUserControl11.BringToFront();
            
        }

        private void driverToTrainUserControl11_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (System.Windows.Forms.Application.MessageLoop)
            {
                System.Windows.Forms.Application.Exit();
            }
            else
            {
                System.Environment.Exit(1);
            }
        }

        private void deleteTripUserControl11_Load(object sender, EventArgs e)
        {

        }

        private void viewLoginUserControl11_Load(object sender, EventArgs e)
        {

        }

        private void driverInfoUserControl11_Load(object sender, EventArgs e)
        {

        }



    }
}
