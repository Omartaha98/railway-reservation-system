﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace LoginUser
{
    public partial class PurchaseTicketUserControl1 : UserControl
    {
        public PurchaseTicketUserControl1()
        {
            InitializeComponent();
        }
        int indexRow;
        int UserID;
        string UserName;
        int count = 0;
        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox2.Text == "1" || textBox2.Text == "2" || textBox2.Text == "3")
                //if credit card number equals 1 or 2 or 3 then it is valid
            {

                SqlConnection conn = new SqlConnection("Data Source=DESKTOP-REM3Q7R\\OMAR_TAHA;Initial Catalog=RailwayReservationSystem;Integrated Security=True");
                conn.Open();
                SqlCommand cmd = new SqlCommand("select Customer_Name,Customer_ID from Customer where UserName=@usrnm", conn);
                SqlParameter userName = new SqlParameter("@usrnm", textBox3.Text);
                cmd.Parameters.Add(userName);

                SqlDataReader reader1 = cmd.ExecuteReader();

                while (reader1.Read())
                {
                    UserName= reader1.GetString(0);
                    UserID = reader1.GetInt32(1);
                    count++;
                }
                reader1.Close();
                conn.Close();
                SqlConnection conn1 = new SqlConnection("Data Source=DESKTOP-REM3Q7R\\OMAR_TAHA;Initial Catalog=RailwayReservationSystem;Integrated Security=True");
                conn1.Open();
                SqlCommand cmd2 = new SqlCommand("insert into Purchase(Customer_ID,Trip_ID,Customer_Name) values (@varCust_ID,@varTrip_ID,@varCust_Nm)", conn1);
                SqlParameter parCust_ID = new SqlParameter("@varCust_ID", UserID);
                cmd2.Parameters.Add(parCust_ID);
                SqlParameter parTrip_ID = new SqlParameter("@varTrip_ID", textBox1.Text);
                cmd2.Parameters.Add(parTrip_ID);
                SqlParameter parCust_Nm = new SqlParameter("@varCust_Nm", UserName);
                cmd2.Parameters.Add(parCust_Nm);
                cmd2.ExecuteNonQuery();
                conn1.Close();
                if (count == 1)
                {
                    MessageBox.Show("You complete Purchasing!", ("Sucessful Purchasing"), MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            indexRow = e.RowIndex;
            DataGridViewRow row = dataGridView1.Rows[indexRow];
            textBox1.Text = row.Cells[0].Value.ToString();
        }

        private void PurchaseTicketUserControl1_Load(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-REM3Q7R\\OMAR_TAHA;Initial Catalog=RailwayReservationSystem;Integrated Security=True");
            conn.Open();
            SqlCommand cmd = new SqlCommand("select * from Trip", conn);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable T = new DataTable();
            T.Columns.Add("TripNumber");
            T.Columns.Add("DepartCity");
            T.Columns.Add("ArrivalCity");
            T.Columns.Add("DepartTime");
            T.Columns.Add("ArrivalTime");
            T.Columns.Add("DepartDate");
            T.Columns.Add("TicketPrice");
            T.Columns.Add("TrainNumber");
            DataRow row;
            while (reader.Read())
            {
                row = T.NewRow();
                row["TripNumber"] = reader["Trip_Number"];
                row["DepartCity"] = reader["DepartialCity"];
                row["ArrivalCity"] = reader["ArrivalCity"];
                row["DepartTime"] = reader["DepartTime"];
                row["ArrivalTime"] = reader["ArrivalTime"];
                row["DepartDate"] = reader["DepartDate"];
                row["TicketPrice"] = reader["Ticket_Price"];
                row["TrainNumber"] = reader["Train_ID"];
                T.Rows.Add(row);
            }

            reader.Close();
            conn.Close();
            dataGridView1.DataSource = T;
            dataGridView1.BorderStyle = BorderStyle.None;
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridView1.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            dataGridView1.BackgroundColor = Color.White;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(20, 25, 72);
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-REM3Q7R\\OMAR_TAHA;Initial Catalog=RailwayReservationSystem;Integrated Security=True");
            conn.Open();
            SqlCommand cmd = new SqlCommand("select * from Trip", conn);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable T = new DataTable();
            T.Columns.Add("TripNumber");
            T.Columns.Add("DepartCity");
            T.Columns.Add("ArrivalCity");
            T.Columns.Add("DepartTime");
            T.Columns.Add("ArrivalTime");
            T.Columns.Add("DepartDate");
            T.Columns.Add("TicketPrice");
            T.Columns.Add("TrainNumber");
            DataRow row;
            while (reader.Read())
            {
                row = T.NewRow();
                row["TripNumber"] = reader["Trip_Number"];
                row["DepartCity"] = reader["DepartialCity"];
                row["ArrivalCity"] = reader["ArrivalCity"];
                row["DepartTime"] = reader["DepartTime"];
                row["ArrivalTime"] = reader["ArrivalTime"];
                row["DepartDate"] = reader["DepartDate"];
                row["TicketPrice"] = reader["Ticket_Price"];
                row["TrainNumber"] = reader["Train_ID"];
                T.Rows.Add(row);
            }

            reader.Close();
            conn.Close();
            dataGridView1.DataSource = T;
            dataGridView1.BorderStyle = BorderStyle.None;
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridView1.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            dataGridView1.BackgroundColor = Color.White;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(20, 25, 72);
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
