﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace LoginUser
{
    public partial class UpdateTripUserControl1 : UserControl
    {
        public UpdateTripUserControl1()
        {
            InitializeComponent();
        }
        int indexRow;
        private void UpdateTripUserControl1_Load(object sender, EventArgs e)
        {

            SqlConnection con = new SqlConnection("Data Source=DESKTOP-EIQMB92;Initial Catalog=NewUserLogin;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Trip", con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable T = new DataTable();
            T.Columns.Add("TripNumber");
            T.Columns.Add("DepartCity");
            T.Columns.Add("ArrivalCity");
            T.Columns.Add("DepartTime");
            T.Columns.Add("ArrivalTime");
            T.Columns.Add("DepartDate");
            T.Columns.Add("TicketPrice");
            T.Columns.Add("TrainNumber");
            DataRow row;
            while (reader.Read())
            {
                row = T.NewRow();
                row["TripNumber"] = reader["Trip_Number"];
                row["DepartCity"] = reader["DepartialCity"];
                row["ArrivalCity"] = reader["ArrivalCity"];
                row["DepartTime"] = reader["DepartTime"];
                row["ArrivalTime"] = reader["ArrivalTime"];
                row["DepartDate"] = reader["DepartDate"];
                row["TicketPrice"] = reader["Ticket_Price"];
                row["TrainNumber"] = reader["Train_ID"];
                T.Rows.Add(row);
            }

            reader.Close();
            con.Close();
            dataGridView1.DataSource = T;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            indexRow = e.RowIndex;
            DataGridViewRow row = dataGridView1.Rows[indexRow];
            textBox1.Text = row.Cells[0].Value.ToString();
            textBox2.Text = row.Cells[1].Value.ToString();
            textBox3.Text = row.Cells[2].Value.ToString();
            dateTimePicker1.Text = row.Cells[3].Value.ToString();
            dateTimePicker2.Text = row.Cells[4].Value.ToString();
            dateTimePicker3.Text = row.Cells[5].Value.ToString();
            textBox7.Text = row.Cells[0].Value.ToString();
            textBox8.Text = row.Cells[0].Value.ToString();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=DESKTOP-EIQMB92;Initial Catalog=NewUserLogin;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Trip", con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable T = new DataTable();
            T.Columns.Add("TripNumber");
            T.Columns.Add("DepartCity");
            T.Columns.Add("ArrivalCity");
            T.Columns.Add("DepartTime");
            T.Columns.Add("ArrivalTime");
            T.Columns.Add("DepartDate");
            T.Columns.Add("TicketPrice");
            T.Columns.Add("TrainNumber");
            DataRow row;
            while (reader.Read())
            {
                row = T.NewRow();
                row["TripNumber"] = reader["Trip_Number"];
                row["DepartCity"] = reader["DepartialCity"];
                row["ArrivalCity"] = reader["ArrivalCity"];
                row["DepartTime"] = reader["DepartTime"];
                row["ArrivalTime"] = reader["ArrivalTime"];
                row["DepartDate"] = reader["DepartDate"];
                row["TicketPrice"] = reader["Ticket_Price"];
                row["TrainNumber"] = reader["Train_ID"];
                T.Rows.Add(row);
            }

            reader.Close();
            con.Close();
            dataGridView1.DataSource = T;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int count = 0;
            try
            {
                if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox7.Text == "" || textBox8.Text == "" )
                {
                    MessageBox.Show("No entered elements!", ("Missing Inputs"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    SqlConnection con1 = new SqlConnection("Data Source=DESKTOP-EIQMB92;Initial Catalog=NewUserLogin;Integrated Security=True");
                    con1.Open();
                    SqlCommand cmd1 = new SqlCommand("select Trip_Number from Trip where Trip_Number = @varTripNumber", con1);
                    cmd1.CommandType = CommandType.Text;
                    SqlParameter parTripNumber = new SqlParameter("@varTripNumber", textBox1.Text);
                    cmd1.Parameters.Add(parTripNumber);
                    SqlDataReader reader = cmd1.ExecuteReader();
                    while (reader.Read())
                    {
                        count += 1;
                    }

                    reader.Close();
                    con1.Close();
                    if (count == 1)
                    {
                        SqlConnection con = new SqlConnection("Data Source=DESKTOP-EIQMB92;Initial Catalog=NewUserLogin;Integrated Security=True");
                        con.Open();
                        SqlCommand cmd = new SqlCommand("update Trip set DepartialCity= @varDepartialCity,ArrivalCity = @varArrivalCity,ArrivalTime= @varArrivalTime,DepartTime= @varDepartTime,DepartDate = @varDepartDate,Ticket_Price = @varTicket_Price,Train_ID = @varTrain_ID where Trip_Number = @varTrip_Number;", con);
                        SqlParameter parTrip_Number = new SqlParameter("@varTrip_Number", textBox1.Text);
                        cmd.Parameters.Add(parTrip_Number);
                        SqlParameter parDepartialCity = new SqlParameter("@varDepartialCity", textBox2.Text);
                        cmd.Parameters.Add(parDepartialCity);
                        SqlParameter parArrivalCity = new SqlParameter("@varArrivalCity", textBox3.Text);
                        cmd.Parameters.Add(parArrivalCity);
                        SqlParameter parArrivalTime = new SqlParameter("@varArrivalTime", dateTimePicker1.Text);
                        cmd.Parameters.Add(parArrivalTime);
                        SqlParameter parDepartTime = new SqlParameter("@varDepartTime", dateTimePicker2.Text);
                        cmd.Parameters.Add(parDepartTime);
                        SqlParameter parDepartDate = new SqlParameter("@varDepartDate", dateTimePicker3.Text);
                        cmd.Parameters.Add(parDepartDate);
                        SqlParameter parTrain_ID = new SqlParameter("@varTrain_ID", textBox8.Text);
                        cmd.Parameters.Add(parTrain_ID);
                        SqlParameter parTicket_Price = new SqlParameter("@varTicket_Price", textBox7.Text);
                        cmd.Parameters.Add(parTicket_Price);
                        cmd.ExecuteNonQuery();
                        con.Close();
                        MessageBox.Show("You have updated Successful!", ("Successful update"), MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Trip Number Doesn't Exist!", ("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }   catch(Exception)
            {
                MessageBox.Show("Invalid input!", ("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        private void textBox8_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        private void textBox7_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        }

        
    }

