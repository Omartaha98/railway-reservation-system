﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace LoginUser
{
    public partial class DriverToTrainUserControl1 : UserControl
    {
        public DriverToTrainUserControl1()
        {
            InitializeComponent();
        }
        int indexRow;
        private void DriverToTrainUserControl1_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=DESKTOP-REM3Q7R\\OMAR_TAHA;Initial Catalog=RailwayReservationSystem;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Driver_Train  ", con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable T = new DataTable();
            T.Columns.Add("TrainID");
            T.Columns.Add("DriverID");

            DataRow row;
            while (reader.Read())
            {
                row = T.NewRow();
                row["TrainID"] = reader["Train_ID"];
                row["DriverID"] = reader["Driver_ID"];

                T.Rows.Add(row);
            }
            reader.Close();
            con.Close();
            dataGridView1.DataSource = T;
            dataGridView1.BorderStyle = BorderStyle.None;
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridView1.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise; dataGridView1.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            dataGridView1.BackgroundColor = Color.White; dataGridView1.EnableHeadersVisualStyles = false; 
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None; 
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(20, 25, 72);
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=DESKTOP-REM3Q7R\\OMAR_TAHA;Initial Catalog=RailwayReservationSystem;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Driver_Train ", con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable T = new DataTable();
            T.Columns.Add("TrainID");
            T.Columns.Add("DriverID");

            DataRow row;
            while (reader.Read())
            {
                row = T.NewRow();
                row["TrainID"] = reader["Train_ID"];
                row["DriverID"] = reader["Driver_ID"];

                T.Rows.Add(row);
            }
            reader.Close();
            con.Close();
            dataGridView1.DataSource = T;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text == "")
            {
                MessageBox.Show("Empty sets!", ("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            else
            {
                try
                {
                    SqlConnection con = new SqlConnection("Data Source=DESKTOP-REM3Q7R\\OMAR_TAHA;Initial Catalog=RailwayReservationSystem;Integrated Security=True");
                    con.Open();
                    SqlCommand cmd = new SqlCommand("insert into Driver_Train (Train_ID,Driver_ID) values ( @varTID , @varDID ) ", con);
                    SqlParameter parDID = new SqlParameter("@varDID", textBox1.Text);
                    cmd.Parameters.Add(parDID);
                    SqlParameter parTID = new SqlParameter("@varTID", textBox2.Text);
                    cmd.Parameters.Add(parTID);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Sucess!", ("Sucess"), MessageBoxButtons.OK, MessageBoxIcon.Information);
                    con.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("ID does not exist!", ("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            indexRow = e.RowIndex;
            DataGridViewRow row = dataGridView1.Rows[indexRow];
            textBox2.Text = row.Cells[0].Value.ToString();
            
        }
    }
}
