﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace LoginUser
{
    public partial class ReviewTripUserControl1 : UserControl
    {
        public ReviewTripUserControl1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=DESKTOP-REM3Q7R\\OMAR_TAHA;Initial Catalog=RailwayReservationSystem;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("Update Purchase set Review=@Reviews where Purchase_ID=@PurID", con);
            SqlParameter rev = new SqlParameter("@Reviews", textBox1.Text);
            cmd.Parameters.Add(rev);
            SqlParameter Pur_ID = new SqlParameter("@PurID", textBox2.Text);
            cmd.Parameters.Add(Pur_ID);
            cmd.ExecuteNonQuery();
            con.Close();
            MessageBox.Show("Successful Review!!", ("Review Complete"), MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
