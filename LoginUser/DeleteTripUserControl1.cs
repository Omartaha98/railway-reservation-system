﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace LoginUser
{
    public partial class DeleteTripUserControl1 : UserControl
    {
        public DeleteTripUserControl1()
        {
            InitializeComponent();
        }
        int indexRow;
        private void DeleteTripUserControl1_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=DESKTOP-REM3Q7R\\OMAR_TAHA;Initial Catalog=RailwayReservationSystem;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Trip", con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable T = new DataTable();
            T.Columns.Add("TripNumber");
            T.Columns.Add("DepartCity");
            T.Columns.Add("ArrivalCity");
            T.Columns.Add("DepartTime");
            T.Columns.Add("ArrivalTime");
            T.Columns.Add("DepartDate");
            T.Columns.Add("TicketPrice");
            T.Columns.Add("TrainNumber");
            DataRow row;
            while(reader.Read())
            {
                row = T.NewRow();
                row["TripNumber"] = reader["Trip_Number"];
                row["DepartCity"] = reader["DepartialCity"];
                row["ArrivalCity"] = reader["ArrivalCity"];
                row["DepartTime"] = reader["DepartTime"];
                row["ArrivalTime"]= reader["ArrivalTime"];
                row["DepartDate"] = reader["DepartDate"];
                row["TicketPrice"] = reader["Ticket_Price"];
                row["TrainNumber"] = reader["Train_ID"];
                T.Rows.Add(row);
            }

            reader.Close();
            con.Close();
            dataGridView1.DataSource = T;
            dataGridView1.BorderStyle = BorderStyle.None; dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249); dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal; dataGridView1.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise; dataGridView1.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke; dataGridView1.BackgroundColor = Color.White; dataGridView1.EnableHeadersVisualStyles = false; dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None; dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(20, 25, 72); dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;

        }
    
        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=DESKTOP-REM3Q7R\\OMAR_TAHA;Initial Catalog=RailwayReservationSystem;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Trip", con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable T = new DataTable();
            T.Columns.Add("TripNumber");
            T.Columns.Add("DepartCity");
            T.Columns.Add("ArrivalCity");
            T.Columns.Add("DepartTime");
            T.Columns.Add("ArrivalTime");
            T.Columns.Add("DepartDate");
            T.Columns.Add("TicketPrice");
            T.Columns.Add("TrainNumber");
            DataRow row;
            while(reader.Read())
            {
                row = T.NewRow();
                row["TripNumber"] = reader["Trip_Number"];
                row["DepartCity"] = reader["DepartialCity"];
                row["ArrivalCity"] = reader["ArrivalCity"];
                row["DepartTime"] = reader["DepartTime"];
                row["ArrivalTime"]= reader["ArrivalTime"];
                row["DepartDate"] = reader["DepartDate"];
                row["TicketPrice"] = reader["Ticket_Price"];
                row["TrainNumber"] = reader["Train_ID"];
                T.Rows.Add(row);
            }

            reader.Close();
            con.Close();
            dataGridView1.DataSource = T;
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            int count = 0;
            try
            {

                SqlConnection con = new SqlConnection("Data Source=DESKTOP-REM3Q7R\\OMAR_TAHA;Initial Catalog=RailwayReservationSystem;Integrated Security=True");
                con.Open();
                SqlCommand cmd = new SqlCommand("select Trip_Number from Trip where Trip_Number = @varTripNumber", con);
                cmd.CommandType = CommandType.Text;
                SqlParameter parTripNumber = new SqlParameter("@varTripNumber", textBox1.Text);
                cmd.Parameters.Add(parTripNumber);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    count += 1;
                }
                
                reader.Close();
                con.Close();
                if (count == 1)
                {
                    SqlConnection con1 = new SqlConnection("Data Source=DESKTOP-REM3Q7R\\OMAR_TAHA;Initial Catalog=RailwayReservationSystem;Integrated Security=True");
                    con1.Open();
                    SqlCommand cmd1 = new SqlCommand("delete from Trip where Trip_Number = @varTripNumber1", con1);
                    SqlParameter parTripNumber1 = new SqlParameter("@varTripNumber1", textBox1.Text);
                    cmd1.Parameters.Add(parTripNumber1);
                    cmd1.ExecuteNonQuery();
                    con1.Close();
                    MessageBox.Show("You have deleted the trip successfully!", ("Mission Completed"), MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Trip doesn't exist", ("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid inputs!", ("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if(!Char.IsDigit(ch) && ch!=8)
            {
                e.Handled = true;
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            indexRow = e.RowIndex;
            DataGridViewRow row = dataGridView1.Rows[indexRow];
            textBox1.Text = row.Cells[0].Value.ToString();
        }

    
        
    }
 }
   
