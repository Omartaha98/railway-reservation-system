﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace LoginUser
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {
        
                if (new_user.Text == "" || new_password.Text == "")
                {
                    MessageBox.Show("No entered elements!", ("Missing Inputs"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    try
                    {
                        SqlConnection con = new SqlConnection("Data Source=DESKTOP-EIQMB92;Initial Catalog=NewUserLogin;Integrated Security=True");
                        con.Open();
                        SqlCommand cmd = new SqlCommand("insert into UserLogin (UserName , Password , Role) values (@username,@password , @role)", con);
                        SqlParameter paramuser = new SqlParameter("@username", new_user.Text);
                        cmd.Parameters.Add(paramuser);
                        SqlParameter parampassword = new SqlParameter("@password", new_password.Text);
                        cmd.Parameters.Add(parampassword);
                        SqlParameter parrole = new SqlParameter("@role", role_box.Text);
                        cmd.Parameters.Add(parrole);
                        cmd.ExecuteNonQuery();
                        con.Close();

                        if (role_box.Text == "Driver")
                        {
                            SqlConnection con1 = new SqlConnection("Data Source=DESKTOP-EIQMB92;Initial Catalog=NewUserLogin;Integrated Security=True");
                            con1.Open();
                            SqlCommand cmd1 = new SqlCommand("insert into Driver (Driver_Name , Phone_Number , Age , Driver_ID , UserName) values (@varName,@varPhone , @varAge , @varID , @varUsrn)", con1);
                            SqlParameter parName = new SqlParameter("@varName", textBox3.Text);
                            cmd1.Parameters.Add(parName);
                            SqlParameter parPhone = new SqlParameter("@varPhone", textBox1.Text);
                            cmd1.Parameters.Add(parPhone);
                            SqlParameter parAge = new SqlParameter("@varAge", textBox4.Text);
                            cmd1.Parameters.Add(parAge);
                            SqlParameter parID = new SqlParameter("@varID", textBox2.Text);
                            cmd1.Parameters.Add(parID);
                            SqlParameter parUsrn = new SqlParameter("@varUsrn", new_user.Text);
                            cmd1.Parameters.Add(parUsrn);

                            cmd1.ExecuteNonQuery();

                            con1.Close();
                        }
                        if (role_box.Text == "Admin")
                        {
                            SqlConnection con2 = new SqlConnection("Data Source=DESKTOP-EIQMB92;Initial Catalog=NewUserLogin;Integrated Security=True");
                            con2.Open();
                            SqlCommand cmd2 = new SqlCommand("insert into Admin (Admin_Name , Phone_Number , Age , Admin_ID , UserName) values (@varName,@varPhone , @varAge , @varID , @varUsrn)", con2);
                            SqlParameter parName = new SqlParameter("@varName", textBox3.Text);
                            cmd2.Parameters.Add(parName);
                            SqlParameter parPhone = new SqlParameter("@varPhone", textBox1.Text);
                            cmd2.Parameters.Add(parPhone);
                            SqlParameter parAge = new SqlParameter("@varAge", textBox4.Text);
                            cmd2.Parameters.Add(parAge);
                            SqlParameter parID = new SqlParameter("@varID", textBox2.Text);
                            cmd2.Parameters.Add(parID);
                            SqlParameter parUsrn = new SqlParameter("@varUsrn", new_user.Text);
                            cmd2.Parameters.Add(parUsrn);
                            cmd2.ExecuteNonQuery();

                            con2.Close();
                        }
                        if (role_box.Text == "User")
                        {
                            SqlConnection con3 = new SqlConnection("Data Source=DESKTOP-EIQMB92;Initial Catalog=NewUserLogin;Integrated Security=True");
                            con3.Open();
                            SqlCommand cmd3 = new SqlCommand("insert into Customer (Customer_Name , Phone_Number , Age , Customer_ID , UserName) values (@varName,@varPhone , @varAge , @varID , @varUsrn)", con3);
                            SqlParameter parName = new SqlParameter("@varName", textBox3.Text);
                            cmd3.Parameters.Add(parName);
                            SqlParameter parPhone = new SqlParameter("@varPhone", textBox1.Text);
                            cmd3.Parameters.Add(parPhone);
                            SqlParameter parAge = new SqlParameter("@varAge", textBox4.Text);
                            cmd3.Parameters.Add(parAge);
                            SqlParameter parID = new SqlParameter("@varID", textBox2.Text);
                            cmd3.Parameters.Add(parID);
                            SqlParameter parUsrn = new SqlParameter("@varUsrn", new_user.Text);
                            cmd3.Parameters.Add(parUsrn);
                            cmd3.ExecuteNonQuery();

                            con3.Close();
                        }

                        MessageBox.Show("Account has been created sucessfully!", ("Created Sucessful!"), MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Hide();
                        Form1 f1 = new Form1();
                        f1.ShowDialog();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Invalid Inputs!", ("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            
        }
        private void new_user_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = (e.KeyChar == (char)Keys.Space);
        }

        private void new_password_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = (e.KeyChar == (char)Keys.Space);

        }

        private void Form2_Load(object sender, EventArgs e)
        {
            role_box.SelectedIndex = 0;
        }

        

        

        



       
    }
}
