﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace LoginUser
{
    public partial class ViewRatingUserControl1 : UserControl
    {
        public ViewRatingUserControl1()
        {
            InitializeComponent();
        }

        private void ViewRatingUserControl1_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=DESKTOP-REM3Q7R\\OMAR_TAHA;Initial Catalog=RailwayReservationSystem;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("select Purchase_ID , Customer_ID , Trip_ID , Customer_Name , Rating from Purchase ", con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable T = new DataTable();
            T.Columns.Add("PurchaseID");
            T.Columns.Add("CustomerID");
            T.Columns.Add("CustomerName");
            T.Columns.Add("TripID");
            T.Columns.Add("Rating");
            DataRow row;
            while (reader.Read())
            {
                row = T.NewRow();
                row["PurchaseID"] = reader["Purchase_ID"];
                row["CustomerID"] = reader["Customer_ID"];
                row["CustomerName"] = reader["Customer_Name"];
                row["TripID"] = reader["Trip_ID"];
                row["Rating"] = reader["Rating"];
                
                T.Rows.Add(row);
            }

            reader.Close();
            con.Close();
            dataGridView1.DataSource = T;

            dataGridView1.BorderStyle = BorderStyle.None;
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridView1.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            dataGridView1.BackgroundColor = Color.White;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(20, 25, 72);
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=DESKTOP-REM3Q7R\\OMAR_TAHA;Initial Catalog=RailwayReservationSystem;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("select Purchase_ID , Customer_ID , Trip_ID , Customer_Name , Rating from Purchase ", con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable T = new DataTable();
            T.Columns.Add("PurchaseID");
            T.Columns.Add("CustomerID");
            T.Columns.Add("CustomerName");
            T.Columns.Add("TripID");
            T.Columns.Add("Rating");
            DataRow row;
            while (reader.Read())
            {
                row = T.NewRow();
                row["PurchaseID"] = reader["Purchase_ID"];
                row["CustomerID"] = reader["Customer_ID"];
                row["CustomerName"] = reader["Customer_Name"];
                row["TripID"] = reader["Trip_ID"];
                row["Rating"] = reader["Rating"];

                T.Rows.Add(row);
            }

            reader.Close();
            con.Close();
            dataGridView1.DataSource = T;

            dataGridView1.BorderStyle = BorderStyle.None;
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridView1.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            dataGridView1.BackgroundColor = Color.White;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(20, 25, 72);
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form6 f = new Form6();
            f.ShowDialog();
        }
    }
}
