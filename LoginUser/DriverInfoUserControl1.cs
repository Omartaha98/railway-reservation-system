﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace LoginUser
{
    public partial class DriverInfoUserControl1 : UserControl
    {
        public DriverInfoUserControl1()
        {
            InitializeComponent();
        }

        private void DriverInfoUserControl1_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=DESKTOP-REM3Q7R\\OMAR_TAHA;Initial Catalog=RailwayReservationSystem;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Driver ", con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable T = new DataTable();
            T.Columns.Add("DriverID");
            T.Columns.Add("DriverName");
            T.Columns.Add("StartShift");
            T.Columns.Add("EndShift");
            T.Columns.Add("ShiftDate");
            T.Columns.Add("PhoneNumber");
            T.Columns.Add("Age");
            T.Columns.Add("UserName");
            DataRow row;
            while (reader.Read())
            {
                row = T.NewRow();
                row["DriverID"] = reader["Driver_ID"];
                row["DriverName"] = reader["Driver_Name"];
                row["StartShift"] = reader["StartShift"];
                row["EndShift"] = reader["EndShift"];
                row["ShiftDate"] = reader["ShiftDate"];
                row["PhoneNumber"] = reader["Phone_Number"];
                row["Age"] = reader["Age"];
                row["UserName"] = reader["UserName"];
                T.Rows.Add(row);
            }
            reader.Close();
            con.Close();
            dataGridView1.DataSource = T;
            dataGridView1.BorderStyle = BorderStyle.None;
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridView1.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            dataGridView1.BackgroundColor = Color.White;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(20, 25, 72);
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=DESKTOP-REM3Q7R\\OMAR_TAHA;Initial Catalog=RailwayReservationSystem;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Driver ", con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable T = new DataTable();
            T.Columns.Add("DriverID");
            T.Columns.Add("DriverName");
            T.Columns.Add("StartShift");
            T.Columns.Add("EndShift");
            T.Columns.Add("ShiftDate");
            T.Columns.Add("PhoneNumber");
            T.Columns.Add("Age");
            T.Columns.Add("UserName");
            DataRow row;
            while (reader.Read())
            {
                row = T.NewRow();
                row["DriverID"] = reader["Driver_ID"];
                row["DriverName"] = reader["Driver_Name"];
                row["StartShift"] = reader["StartShift"];
                row["EndShift"] = reader["EndShift"];
                row["ShiftDate"] = reader["ShiftDate"];
                row["PhoneNumber"] = reader["Phone_Number"];
                row["Age"] = reader["Age"];
                row["UserName"] = reader["UserName"];
                T.Rows.Add(row);
            }
            reader.Close();
            con.Close();
            dataGridView1.DataSource = T;
            dataGridView1.BorderStyle = BorderStyle.None;
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridView1.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            dataGridView1.BackgroundColor = Color.White;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(20, 25, 72);
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form6 f = new Form6();
            f.ShowDialog();
        }
    }
}
