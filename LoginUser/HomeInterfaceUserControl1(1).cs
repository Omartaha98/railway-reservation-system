﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoginUser
{
    public partial class HomeInterfaceUserControl1 : UserControl
    {
        public HomeInterfaceUserControl1()
        {
            this.BackgroundImage = Properties.Resources.pbox;
            InitializeComponent();
            Timer tm = new Timer();
            tm.Interval = 5000;
            tm.Tick += new EventHandler(changeImage);
            tm.Start();

        }

        private void changeImage(object sender, EventArgs e)
        {
            List<Bitmap> b1 = new List<Bitmap>();
            b1.Add(Properties.Resources.pbox);
            b1.Add(Properties.Resources.slider01);
            b1.Add(Properties.Resources.slider02);
            b1.Add(Properties.Resources.slider03);
            int index = DateTime.Now.Second % b1.Count;
            this.BackgroundImage = b1[index];


        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

       

      
    }
}
