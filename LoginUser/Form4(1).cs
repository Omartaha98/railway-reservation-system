﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoginUser
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SidePanel1.Top = panelDropDown.Top;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            SidePanel1.Height = button2.Height;
            SidePanel1.Top = button2.Top;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SidePanel1.Height = button3.Height;
            SidePanel1.Top = button3.Top;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            SidePanel1.Height = button4.Height;
            SidePanel1.Top = button4.Top;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            SidePanel1.Height = button5.Height;
            SidePanel1.Top = button5.Top;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            SidePanel1.Height = button6.Height;
            SidePanel1.Top = button6.Top;
        }

        private void button13_Click(object sender, EventArgs e)
        {
            if (System.Windows.Forms.Application.MessageLoop)
            {
                System.Windows.Forms.Application.Exit();
            }
            else
            {
                System.Environment.Exit(1);
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are You sure To Log Out?", ("Log Out"), MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
            {
                this.Hide();
                Form1 f = new Form1();

                f.ShowDialog();
            }
           
        }

        
    }
}
