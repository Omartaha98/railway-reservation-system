﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace LoginUser
{
    public partial class UpdateTrainTimeUserControl1 : UserControl
    {
        public UpdateTrainTimeUserControl1()
        {
            InitializeComponent();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            int count = 0;
            try
            {
                if (textBox1.Text == "")
                {
                    MessageBox.Show("No entered elements!", ("Missing Inputs"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    SqlConnection con1 = new SqlConnection("Data Source=DESKTOP-REM3Q7R\\OMAR_TAHA;Initial Catalog=RailwayReservationSystem;Integrated Security=True");
                    con1.Open();
                    SqlCommand cmd1 = new SqlCommand("select Trip_Number from Trip where Trip_Number = @varTripNumber", con1);
                    cmd1.CommandType = CommandType.Text;
                    SqlParameter parTripNumber = new SqlParameter("@varTripNumber", textBox1.Text);
                    cmd1.Parameters.Add(parTripNumber);
                    SqlDataReader reader = cmd1.ExecuteReader();
                    while (reader.Read())
                    {
                        count += 1;
                    }

                    reader.Close();
                    con1.Close();
                    if (count == 1)
                    {
                        SqlConnection con = new SqlConnection("Data Source=DESKTOP-REM3Q7R\\OMAR_TAHA;Initial Catalog=RailwayReservationSystem;Integrated Security=True");
                        con.Open();
                        SqlCommand cmd = new SqlCommand("update Trip set ArrivalTime= @varArrivalTime,DepartTime= @varDepartTime,DepartDate = @varDepartDate where Trip_Number = @varTrip_Number;", con);
                        SqlParameter parArrivalTime = new SqlParameter("@varArrivalTime", TimePicker1.Text);
                        cmd.Parameters.Add(parArrivalTime);
                        SqlParameter parDepartTime = new SqlParameter("@varDepartTime", TimePicker2.Text);
                        cmd.Parameters.Add(parDepartTime);
                        SqlParameter parDepartDate = new SqlParameter("@varDepartDate", datePicker3.Text);
                        cmd.Parameters.Add(parDepartDate);
                        SqlParameter TripNumber = new SqlParameter("@varTrip_Number", textBox1.Text);
                        cmd.Parameters.Add(TripNumber);
                        cmd.ExecuteNonQuery();
                        con.Close();
                        MessageBox.Show("You have updated Successful!", ("Successful update"), MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Trip Number Doesn't Exist!", ("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid input!", ("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void UpdateTrainTimeUserControl1_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=DESKTOP-REM3Q7R\\OMAR_TAHA;Initial Catalog=RailwayReservationSystem;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Trip", con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable T = new DataTable();
            T.Columns.Add("TripNumber");
            T.Columns.Add("DepartCity");
            T.Columns.Add("ArrivalCity");
            T.Columns.Add("DepartTime");
            T.Columns.Add("ArrivalTime");
            T.Columns.Add("DepartDate");
            T.Columns.Add("TicketPrice");
            T.Columns.Add("TrainNumber");
            DataRow row;
            while (reader.Read())
            {
                row = T.NewRow();
                row["TripNumber"] = reader["Trip_Number"];
                row["DepartCity"] = reader["DepartialCity"];
                row["ArrivalCity"] = reader["ArrivalCity"];
                row["DepartTime"] = reader["DepartTime"];
                row["ArrivalTime"] = reader["ArrivalTime"];
                row["DepartDate"] = reader["DepartDate"];
                row["TicketPrice"] = reader["Ticket_Price"];
                row["TrainNumber"] = reader["Train_ID"];
                T.Rows.Add(row);
            }

            reader.Close();
            con.Close();
            dataGridView1.DataSource = T;

            dataGridView1.BorderStyle = BorderStyle.None;
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridView1.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            dataGridView1.BackgroundColor = Color.White;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(20, 25, 72);
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;

        }

        private void button6_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=DESKTOP-REM3Q7R\\OMAR_TAHA;Initial Catalog=RailwayReservationSystem;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Trip", con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable T = new DataTable();
            T.Columns.Add("TripNumber");
            T.Columns.Add("DepartCity");
            T.Columns.Add("ArrivalCity");
            T.Columns.Add("DepartTime");
            T.Columns.Add("ArrivalTime");
            T.Columns.Add("DepartDate");
            T.Columns.Add("TicketPrice");
            T.Columns.Add("TrainNumber");
            DataRow row;
            while (reader.Read())
            {
                row = T.NewRow();
                row["TripNumber"] = reader["Trip_Number"];
                row["DepartCity"] = reader["DepartialCity"];
                row["ArrivalCity"] = reader["ArrivalCity"];
                row["DepartTime"] = reader["DepartTime"];
                row["ArrivalTime"] = reader["ArrivalTime"];
                row["DepartDate"] = reader["DepartDate"];
                row["TicketPrice"] = reader["Ticket_Price"];
                row["TrainNumber"] = reader["Train_ID"];
                T.Rows.Add(row);
            }

            reader.Close();
            con.Close();
            dataGridView1.DataSource = T;

            dataGridView1.BorderStyle = BorderStyle.None;
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridView1.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            dataGridView1.BackgroundColor = Color.White;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(20, 25, 72);
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;

        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int indexRow = e.RowIndex;
            DataGridViewRow row = dataGridView1.Rows[indexRow];
            textBox1.Text = row.Cells[0].Value.ToString();
        }
    }
}
