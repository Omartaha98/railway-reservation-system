﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace LoginUser
{
    public partial class DriverToTrainUserControl1 : UserControl
    {
        public DriverToTrainUserControl1()
        {
            InitializeComponent();
        }
        int indexRow;
        private void DriverToTrainUserControl1_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=DESKTOP-EIQMB92;Initial Catalog=NewUserLogin;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Train ", con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable T = new DataTable();
            T.Columns.Add("TrainID");
            T.Columns.Add("DriverID");

            DataRow row;
            while (reader.Read())
            {
                row = T.NewRow();
                row["TrainID"] = reader["Train_ID"];
                row["DriverID"] = reader["Driver_ID"];

                T.Rows.Add(row);
            }
            reader.Close();
            con.Close();
            dataGridView1.DataSource = T;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=DESKTOP-EIQMB92;Initial Catalog=NewUserLogin;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Train ", con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable T = new DataTable();
            T.Columns.Add("TrainID");
            T.Columns.Add("DriverID");

            DataRow row;
            while (reader.Read())
            {
                row = T.NewRow();
                row["TrainID"] = reader["Train_ID"];
                row["DriverID"] = reader["Driver_ID"];

                T.Rows.Add(row);
            }
            reader.Close();
            con.Close();
            dataGridView1.DataSource = T;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=DESKTOP-EIQMB92;Initial Catalog=NewUserLogin;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("update Train set Driver_ID = @varDID where Train_ID = @varTID", con);
            SqlParameter parDID = new SqlParameter("@varDID", textBox1.Text);
            cmd.Parameters.Add(parDID);
            SqlParameter parTID = new SqlParameter("@varTID", textBox2.Text);
            cmd.Parameters.Add(parTID);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Sucess!", ("Sucess"), MessageBoxButtons.OK, MessageBoxIcon.Information);
            con.Close();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            indexRow = e.RowIndex;
            DataGridViewRow row = dataGridView1.Rows[indexRow];
            textBox2.Text = row.Cells[0].Value.ToString();
            
        }
    }
}
