﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace LoginUser
{
    public partial class PreviousTriosUserControl1 : UserControl
    {
        public PreviousTriosUserControl1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection conn1 = new SqlConnection("Data Source=DESKTOP-REM3Q7R\\OMAR_TAHA;Initial Catalog=RailwayReservationSystem;Integrated Security=True");
            conn1.Open();
            SqlCommand cmd2 = new SqlCommand("select Purchase_ID,Customer_ID,Trip_ID,Customer_Name from Purchase where Customer_ID=@varCust_ID", conn1);
            SqlParameter parCust_ID = new SqlParameter("@varCust_ID", textBox1.Text);
            cmd2.Parameters.Add(parCust_ID);
            cmd2.CommandType = CommandType.Text;
            SqlDataReader reader = cmd2.ExecuteReader();
            DataTable T = new DataTable();
            T.Columns.Add("Purchase_ID");
            T.Columns.Add("Customer_ID");
            T.Columns.Add("Trip_ID");
            T.Columns.Add("Customer_Name");
            DataRow row;
            while (reader.Read())
            {
                row = T.NewRow();
                row["Purchase_ID"] = reader["Purchase_ID"];
                row["Customer_ID"] = reader["Customer_ID"];
                row["Trip_ID"] = reader["Trip_ID"];
                row["Customer_Name"] = reader["Customer_Name"];
                T.Rows.Add(row);
            }

            reader.Close();
            conn1.Close();
            dataGridView1.DataSource = T;
           
            dataGridView1.BorderStyle = BorderStyle.None;
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridView1.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            dataGridView1.BackgroundColor = Color.White;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(20, 25, 72);
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
