﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace LoginUser
{
    public partial class AddTrainUserControl1 : UserControl
    {
        public AddTrainUserControl1()
        {
            InitializeComponent();
        }

        private void DriverToTrainUserControl1_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=DESKTOP-EIQMB92;Initial Catalog=NewUserLogin;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Train", con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable T = new DataTable();
            T.Columns.Add("TrainID");
            T.Columns.Add("DriverID");
            DataRow row;
            while (reader.Read())
            {
                row = T.NewRow();
                row["TrainID"] = reader["Train_ID"];
                row["DriverID"] = reader["Driver_ID"];
                T.Rows.Add(row);
            }
            reader.Close();
            con.Close();
            dataGridView1.DataSource = T;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=DESKTOP-EIQMB92;Initial Catalog=NewUserLogin;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Train ", con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable T = new DataTable();
            T.Columns.Add("TrainID");
            T.Columns.Add("DriverID");

            DataRow row;
            while (reader.Read())
            {
                row = T.NewRow();
                row["TrainID"] = reader["Train_ID"];
                row["DriverID"] = reader["Driver_ID"];

                T.Rows.Add(row);
            }
            reader.Close();
            con.Close();
            dataGridView1.DataSource = T;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection con = new SqlConnection("Data Source=DESKTOP-EIQMB92;Initial Catalog=NewUserLogin;Integrated Security=True");
                con.Open();
                SqlCommand cmd = new SqlCommand("insert into Train (Train_ID,Driver_ID) values (@varTID , 0)", con);
                SqlParameter parTID = new SqlParameter("@varTID", textBox2.Text);
                cmd.Parameters.Add(parTID);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Sucess!", ("Sucess"), MessageBoxButtons.OK, MessageBoxIcon.Information);
                con.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("id is already exist!", ("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
    }
}
