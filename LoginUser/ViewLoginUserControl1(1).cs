﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace LoginUser
{
    public partial class ViewLoginUserControl1 : UserControl
    {
        public ViewLoginUserControl1()
        {
            InitializeComponent();
        }

        private void ViewLoginUserControl1_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=DESKTOP-EIQMB92;Initial Catalog=NewUserLogin;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from UserLogin where Role != 'Admin'  ", con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable T = new DataTable();
            T.Columns.Add("UserName");
            T.Columns.Add("Password");
            T.Columns.Add("Role");
            DataRow row;
            while(reader.Read())
            {
                row = T.NewRow();
                row["UserName"] = reader["UserName"];
                row["Password"] = reader["Password"];
                row["Role"] = reader["Role"];
                T.Rows.Add(row);
            }
            reader.Close();
            con.Close();
            dataGridView1.DataSource = T;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form3 f = new Form3();
            f.ShowDialog();
        }
    }
}
