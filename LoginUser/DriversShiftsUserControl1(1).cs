﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace LoginUser
{
    public partial class DriversShiftsUserControl1 : UserControl
    {
        public DriversShiftsUserControl1()
        {
            InitializeComponent();
        }
        int indexRow;
        private void DriversShiftsUserControl1_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=DESKTOP-EIQMB92;Initial Catalog=NewUserLogin;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("select Driver_ID, Driver_Name, StartShift, EndShift , ShiftDate from Driver where Driver_ID != 0 ", con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable T = new DataTable();
            T.Columns.Add("DriverID");
            T.Columns.Add("DriverName");
            T.Columns.Add("StartShift");
            T.Columns.Add("EndShift");
            T.Columns.Add("ShiftDate");
            DataRow row;
            while (reader.Read())
            {
                row = T.NewRow();
                row["DriverID"] = reader["Driver_ID"];
                row["DriverName"] = reader["Driver_Name"];
                row["StartShift"] = reader["StartShift"];
                row["EndShift"] = reader["EndShift"];
                row["ShiftDate"] = reader["ShiftDate"];
                T.Rows.Add(row);
            }
            reader.Close();
            con.Close();
            dataGridView1.DataSource = T;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=DESKTOP-EIQMB92;Initial Catalog=NewUserLogin;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("select Driver_ID, Driver_Name, StartShift, EndShift , ShiftDate from Driver where Driver_ID != 0 ", con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable T = new DataTable();
            T.Columns.Add("DriverID");
            T.Columns.Add("DriverName");
            T.Columns.Add("StartShift");
            T.Columns.Add("EndShift");
            T.Columns.Add("ShiftDate");
            DataRow row;
            while (reader.Read())
            {
                row = T.NewRow();
                row["DriverID"] = reader["Driver_ID"];
                row["DriverName"] = reader["Driver_Name"];
                row["StartShift"] = reader["StartShift"];
                row["EndShift"] = reader["EndShift"];
                row["ShiftDate"] = reader["ShiftDate"];
                T.Rows.Add(row);
            }
            reader.Close();
            con.Close();
            dataGridView1.DataSource = T;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            indexRow = e.RowIndex;
            DataGridViewRow row = dataGridView1.Rows[indexRow];
            textBox1.Text = row.Cells[0].Value.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {


            try
            {
                if (textBox1.Text == "")
                {
                    MessageBox.Show("Wrong ID!", ("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {


                    SqlConnection con = new SqlConnection("Data Source=DESKTOP-EIQMB92;Initial Catalog=NewUserLogin;Integrated Security=True");
                    con.Open();
                    SqlCommand cmd = new SqlCommand("update Driver set StartShift = @varStart , EndShift = @varEnd , ShiftDate = @varDate where Driver_ID = @varID and Driver_ID != 0", con);
                    SqlParameter parStart = new SqlParameter("@varStart", dateTimePicker1.Text);
                    cmd.Parameters.Add(parStart);
                    SqlParameter parEnd = new SqlParameter("@varEnd", dateTimePicker2.Text);
                    cmd.Parameters.Add(parEnd);
                    SqlParameter parDate = new SqlParameter("@varDate", dateTimePicker3.Text);
                    cmd.Parameters.Add(parDate);
                    SqlParameter parID = new SqlParameter("@varID", textBox1.Text);
                    cmd.Parameters.Add(parID);
                    cmd.ExecuteNonQuery();
                    con.Close();
                    MessageBox.Show("You have set shifts Successfully!", ("Mission Success"), MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            } catch(Exception)
            {
                MessageBox.Show("An Error has occured!", ("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
