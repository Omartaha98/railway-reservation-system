﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace LoginUser
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            
            pass_txt.PasswordChar = '*';
        }

      

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 f2 = new Form2();
            f2.ShowDialog();
        }
        string role_st;

        private void button4_Click(object sender, EventArgs e)
        {
            
        }

        private void user_txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = (e.KeyChar == (char)Keys.Space);

        }

        private void pass_txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = (e.KeyChar == (char)Keys.Space);
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            if (user_txt.Text == "" || pass_txt.Text == "")
            {
                MessageBox.Show("No entered elements!", ("Missing Inputs"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                SqlConnection con = new SqlConnection("Data Source=DESKTOP-REM3Q7R\\OMAR_TAHA;Initial Catalog=RailwayReservationSystem;Integrated Security=True");
                con.Open();
                SqlCommand cmd = new SqlCommand("select * from UserLogin where UserName = @varUser and Password = @varPass ", con);

                SqlParameter paramusr = new SqlParameter("@varUser", user_txt.Text);
                cmd.Parameters.Add(paramusr);
                SqlParameter parampass = new SqlParameter("@varPass", pass_txt.Text);
                cmd.Parameters.Add(parampass);
                SqlDataReader reader = cmd.ExecuteReader();
                int count = 0;
                while (reader.Read())
                {
                    count += 1;
                    role_st = reader.GetString(2);
                }


                if (count == 1)
                {
                    MessageBox.Show("You have login sucessfully!", ("Sucessful login"), MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Hide();
                    if (role_st == "Admin")
                    {

                        Form3 f3 = new Form3();
                        f3.ShowDialog();
                    }
                    else if (role_st == "User")
                    {
                        Form4 f4 = new Form4();
                        f4.ShowDialog();
                    }
                    else if (role_st == "Driver")
                    {
                        this.Hide();
                        Form6 f6 = new Form6();
                        f6.ShowDialog();
                    }
                }

                else
                {
                    MessageBox.Show("Invalid UserName or Password!", ("Invalid inputs"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                reader.Close();
                con.Close();
            }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

            this.Hide();
            Form2 f2 = new Form2();
            f2.ShowDialog();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (System.Windows.Forms.Application.MessageLoop)
            {
                System.Windows.Forms.Application.Exit();
            }
            else
            {
                System.Environment.Exit(1);
            }
        }
        }
    }

