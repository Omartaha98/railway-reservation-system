﻿using LoginUser.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoginUser
{
    public partial class Form6 : Form
    {
        private bool isCollapsed;
        public Form6()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SidePanel1.Top = panelDropDown.Top;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            SidePanel1.Height = button2.Height;
            SidePanel1.Top = button2.Top;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SidePanel1.Height = button3.Height;
            SidePanel1.Top = button3.Top;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            SidePanel1.Height = button4.Height;
            SidePanel1.Top = button4.Top;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            SidePanel1.Height = button5.Height;
            SidePanel1.Top = button5.Top;
        }

        private void button13_Click(object sender, EventArgs e)
        {
            if (System.Windows.Forms.Application.MessageLoop)
            {
                System.Windows.Forms.Application.Exit();
            }
            else
            {
                System.Environment.Exit(1);
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are You sure To Log Out?", ("Log Out"), MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
            {
                this.Hide();
                Form1 f = new Form1();

                f.ShowDialog();
            }
           
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (isCollapsed)
            {
                button1.Image = Resources.Collapse_Arrow_20px;
                panelDropDown.Height += 10;
                if (panelDropDown.Size == panelDropDown.MaximumSize)
                {
                    timer1.Stop();
                    isCollapsed = false;
                }
            }
            else
            {
                button1.Image = Resources.Expand_Arrow_20px;
                panelDropDown.Height -= 10;
                if (panelDropDown.Size == panelDropDown.MinimumSize)
                {
                    timer1.Stop();
                    isCollapsed = true;
                }
            }
        }




    }
}
