﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace LoginUser
{
    public partial class ViewUserControl1 : UserControl
    {
        public ViewUserControl1()
        {
            InitializeComponent();
        }

        private void ViewUserControl1_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=DESKTOP-REM3Q7R\\OMAR_TAHA;Initial Catalog=RailwayReservationSystem;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Trip", con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable T = new DataTable();
            T.Columns.Add("TripNumber");
            T.Columns.Add("DepartCity");
            T.Columns.Add("ArrivalCity");
            T.Columns.Add("DepartTime");
            T.Columns.Add("ArrivalTime");
            T.Columns.Add("DepartDate");
            T.Columns.Add("TicketPrice");
            T.Columns.Add("TrainNumber");
            DataRow row;
            while(reader.Read())
            {
                row = T.NewRow();
                row["TripNumber"] = reader["Trip_Number"];
                row["DepartCity"] = reader["DepartialCity"];
                row["ArrivalCity"] = reader["ArrivalCity"];
                row["DepartTime"] = reader["DepartTime"];
                row["ArrivalTime"]= reader["ArrivalTime"];
                row["DepartDate"] = reader["DepartDate"];
                row["TicketPrice"] = reader["Ticket_Price"];
                row["TrainNumber"] = reader["Train_ID"];
                T.Rows.Add(row);
            }

            reader.Close();
            con.Close();
            dataGridView1.DataSource = T;
            dataGridView1.BorderStyle = BorderStyle.None;
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249); 
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridView1.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            dataGridView1.BackgroundColor = Color.White;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None; 
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(20, 25, 72); 
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form3 f3 = new Form3();
            f3.ShowDialog();
          
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=DESKTOP-REM3Q7R\\OMAR_TAHA;Initial Catalog=RailwayReservationSystem;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from Trip", con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable T = new DataTable();
            T.Columns.Add("TripNumber");
            T.Columns.Add("DepartCity");
            T.Columns.Add("ArrivalCity");
            T.Columns.Add("DepartTime");
            T.Columns.Add("ArrivalTime");
            T.Columns.Add("DepartDate");
            T.Columns.Add("TicketPrice");
            T.Columns.Add("TrainNumber");
            DataRow row;
            while(reader.Read())
            {
                row = T.NewRow();
                row["TripNumber"] = reader["Trip_Number"];
                row["DepartCity"] = reader["DepartialCity"];
                row["ArrivalCity"] = reader["ArrivalCity"];
                row["DepartTime"] = reader["DepartTime"];
                row["ArrivalTime"]= reader["ArrivalTime"];
                row["DepartDate"] = reader["DepartDate"];
                row["TicketPrice"] = reader["Ticket_Price"];
                row["TrainNumber"] = reader["Train_ID"];
                T.Rows.Add(row);
            }

            reader.Close();
            con.Close();
            dataGridView1.DataSource = T;
        }
        }

        
    }

